
Close
{if( CIPFillVLV.boHAutoMan," CIP Fill Valve in Auto", "CIP Fill Valve in Manual")}
Auto Mode Requirements
{if( CIPdrainVLV.boHAutoMan," CIP Drain Valve in Auto", "CIP Drain Valve in Manual")}
{if( CIPsprayVLV.boHAutoMan," CIP Spray Valve in Auto", "CIP Spray Valve in Manual")}
{if( CIPmtr.boHAutoMan," CIP Motor in Auto", "CIP Motor in Manual")}
{if( boGstmAUTOrun,"Auto Mode Running", "System Stopped")}
{if( EDfrnt.boHAutoman,"Front Eductor in Auto", "Front Eductor in Manual")}
{if( EDrear.boHAutoman,"Rear Eductor in Auto", "Rear Eductor in Manual")}
Put All In Auto
{if( HTRfrntTop.boHautoMan,"Front Top Heater in Auto", "Front Top Heater in Manual")}
{if( HTRfrntBOT.boHautoMan,"Front Bot Heater in Auto", "Front Bot Heater in Manual")}
{if( HTRrearTop.boHautoMan,"Rear Top Heater in Auto", "Rear Top Heater in Manual")}
{if( HTRrearBOT.boHautoMan,"Rear Bot Heater in Auto", "Rear Bot Heater in Manual")}
{if( Conv.boHautoman,"Conveyor in Auto", "Conveyor in Manual")}
