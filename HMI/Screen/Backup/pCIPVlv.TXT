

Fill Valve
Status
{if(CIPFillVLV.boDOopn,"OPEN", "CLOSE")}


Auto
Manual


Drain Valve
Status
{if(CIPDrainVlv.boDOopn,"OPEN", "CLOSE")}


Auto
Manual


Return Valve
Status
{if(CIPReturnVLV.boDOopn,"OPEN", "CLOSE")}


Auto
Manual


Spray Valve
Status
{if(CIPSprayVLV.boDOopn,"OPEN", "CLOSE")}


Auto
Manual
Close
