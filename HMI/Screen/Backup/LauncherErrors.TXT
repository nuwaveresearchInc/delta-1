Top Chamber


Power Supply Alarm Setup:
Warning Setpoint (mA)

##.#
Alarm Setpoint (mA)

##.#
Warning Setpoint (kV)

#.#
Alarm Setpoint(kV)

#.#

1
Cmd: {LCRpsCom[1].inHpwrCMD} (%)
Alarm
Va: {LCRpsCom[1].reHVaDuty} (kV)
Ia: {LCRpsCom[1].reHIaDuty} (mA)
HV
PS
E
P

Va: {LCRpsCom[1].reHVaDuty} (kV)
Ia: {LCRpsCom[1].reHIaDuty} (mA)
W
A
Va: {LCRpsCom[1].reHVaDuty} (kV)
Ia: {LCRpsCom[1].reHIaDuty} (mA)
Vf

Va: {LCRpsCom[1].reHVaDuty} (kV)
Ia: {LCRpsCom[1].reHIaDuty} (mA)
Va: {LCRpsCom[1].reHVaDuty} (kV)
Ia: {LCRpsCom[1].reHIaDuty} (mA)
Ia
LL
L
H

2
Cmd: {LCRpsCom[2].inHpwrCMD} (%)
Alarm
Va: {LCRpsCom[2].reHVaDuty} (kV)
Ia: {LCRpsCom[2].reHIaDuty} (mA)
HV
PS
E
P

Va: {LCRpsCom[2].reHVaDuty} (kV)
Ia: {LCRpsCom[2].reHIaDuty} (mA)
W
A
Va: {LCRpsCom[2].reHVaDuty} (kV)
Ia: {LCRpsCom[2].reHIaDuty} (mA)
Vf

Va: {LCRpsCom[2].reHVaDuty} (kV)
Ia: {LCRpsCom[2].reHIaDuty} (mA)
Va: {LCRpsCom[2].reHVaDuty} (kV)
Ia: {LCRpsCom[2].reHIaDuty} (mA)
Ia
LL
L
H

3
Cmd: {LCRpsCom[3].inHpwrCMD} (%)
Alarm
Va: {LCRpsCom[3].reHVaDuty} (kV)
Ia: {LCRpsCom[3].reHIaDuty} (mA)
HV
PS
E
P

Va: {LCRpsCom[3].reHVaDuty} (kV)
Ia: {LCRpsCom[3].reHIaDuty} (mA)
W
A
Va: {LCRpsCom[3].reHVaDuty} (kV)
Ia: {LCRpsCom[3].reHIaDuty} (mA)
Vf

Va: {LCRpsCom[3].reHVaDuty} (kV)
Ia: {LCRpsCom[3].reHIaDuty} (mA)
Va: {LCRpsCom[3].reHVaDuty} (kV)
Ia: {LCRpsCom[3].reHIaDuty} (mA)
Ia
LL
L
H

4
Cmd: {LCRpsCom[4].inHpwrCMD} (%)
Alarm
Va: {LCRpsCom[4].reHVaDuty} (kV)
Ia: {LCRpsCom[4].reHIaDuty} (mA)
HV
PS
E
P

Va: {LCRpsCom[4].reHVaDuty} (kV)
Ia: {LCRpsCom[4].reHIaDuty} (mA)
W
A
Va: {LCRpsCom[4].reHVaDuty} (kV)
Ia: {LCRpsCom[4].reHIaDuty} (mA)
Vf

Va: {LCRpsCom[4].reHVaDuty} (kV)
Ia: {LCRpsCom[4].reHIaDuty} (mA)
Va: {LCRpsCom[4].reHVaDuty} (kV)
Ia: {LCRpsCom[4].reHIaDuty} (mA)
Ia
LL
L
H

5
Cmd: {LCRpsCom[5].inHpwrCMD} (%)
Alarm
Va: {LCRpsCom[5].reHVaDuty} (kV)
Ia: {LCRpsCom[5].reHIaDuty} (mA)
HV
PS
E
P

Va: {LCRpsCom[5].reHVaDuty} (kV)
Ia: {LCRpsCom[5].reHIaDuty} (mA)
W
A
Va: {LCRpsCom[5].reHVaDuty} (kV)
Ia: {LCRpsCom[5].reHIaDuty} (mA)
Vf

Va: {LCRpsCom[5].reHVaDuty} (kV)
Ia: {LCRpsCom[5].reHIaDuty} (mA)
Va: {LCRpsCom[5].reHVaDuty} (kV)
Ia: {LCRpsCom[5].reHIaDuty} (mA)
Ia
LL
L
H

6
Cmd: {LCRpsCom[6].inHpwrCMD} (%)
Alarm
Va: {LCRpsCom[6].reHVaDuty} (kV)
Ia: {LCRpsCom[6].reHIaDuty} (mA)
HV
PS
E
P

Va: {LCRpsCom[6].reHVaDuty} (kV)
Ia: {LCRpsCom[6].reHIaDuty} (mA)
W
A
Va: {LCRpsCom[6].reHVaDuty} (kV)
Ia: {LCRpsCom[6].reHIaDuty} (mA)
Vf

Va: {LCRpsCom[6].reHVaDuty} (kV)
Ia: {LCRpsCom[6].reHIaDuty} (mA)
Va: {LCRpsCom[6].reHVaDuty} (kV)
Ia: {LCRpsCom[6].reHIaDuty} (mA)
Ia
LL
L
H

7
Cmd: {LCRpsCom[7].inHpwrCMD} (%)
Alarm
Va: {LCRpsCom[7].reHVaDuty} (kV)
Ia: {LCRpsCom[7].reHIaDuty} (mA)
HV
PS
E
P

Va: {LCRpsCom[7].reHVaDuty} (kV)
Ia: {LCRpsCom[7].reHIaDuty} (mA)
W
A
Va: {LCRpsCom[7].reHVaDuty} (kV)
Ia: {LCRpsCom[7].reHIaDuty} (mA)
Vf

Va: {LCRpsCom[7].reHVaDuty} (kV)
Ia: {LCRpsCom[7].reHIaDuty} (mA)
Va: {LCRpsCom[7].reHVaDuty} (kV)
Ia: {LCRpsCom[7].reHIaDuty} (mA)
Ia
LL
L
H

8
Cmd: {LCRpsCom[8].inHpwrCMD} (%)
Alarm
Va: {LCRpsCom[8].reHVaDuty} (kV)
Ia: {LCRpsCom[8].reHIaDuty} (mA)
HV
PS
E
P

Va: {LCRpsCom[8].reHVaDuty} (kV)
Ia: {LCRpsCom[8].reHIaDuty} (mA)
W
A
Va: {LCRpsCom[8].reHVaDuty} (kV)
Ia: {LCRpsCom[8].reHIaDuty} (mA)
Vf

Va: {LCRpsCom[8].reHVaDuty} (kV)
Ia: {LCRpsCom[8].reHIaDuty} (mA)
Va: {LCRpsCom[8].reHVaDuty} (kV)
Ia: {LCRpsCom[8].reHIaDuty} (mA)
Ia
LL
L
H

9
Cmd: {LCRpsCom[9].inHpwrCMD} (%)
Alarm
Va: {LCRpsCom[9].reHVaDuty} (kV)
Ia: {LCRpsCom[9].reHIaDuty} (mA)
HV
PS
E
P

Va: {LCRpsCom[9].reHVaDuty} (kV)
Ia: {LCRpsCom[9].reHIaDuty} (mA)
W
A
Va: {LCRpsCom[9].reHVaDuty} (kV)
Ia: {LCRpsCom[9].reHIaDuty} (mA)
Vf

Va: {LCRpsCom[9].reHVaDuty} (kV)
Ia: {LCRpsCom[9].reHIaDuty} (mA)
Va: {LCRpsCom[9].reHVaDuty} (kV)
Ia: {LCRpsCom[9].reHIaDuty} (mA)
Ia
LL
L
H

10
Cmd: {LCRpsCom[10].inHpwrCMD} (%)
Alarm
Va: {LCRpsCom[10].reHVaDuty} (kV)
Ia: {LCRpsCom[10].reHIaDuty} (mA)
HV
PS
E
P

Va: {LCRpsCom[10].reHVaDuty} (kV)
Ia: {LCRpsCom[10].reHIaDuty} (mA)
W
A
Va: {LCRpsCom[10].reHVaDuty} (kV)
Ia: {LCRpsCom[10].reHIaDuty} (mA)
Vf

Va: {LCRpsCom[10].reHVaDuty} (kV)
Ia: {LCRpsCom[10].reHIaDuty} (mA)
Va: {LCRpsCom[10].reHVaDuty} (kV)
Ia: {LCRpsCom[10].reHIaDuty} (mA)
Ia
LL
L
H
Settings
