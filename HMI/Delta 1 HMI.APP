// Application File

[Desktop]
ShowError=1
Title=TRUE
CloseBox=FALSE
MinimizeBox=TRUE
MaximizeBox=TRUE
OpenMaximized=FALSE
MenuBar=FALSE
StatusLine=FALSE
ResizeBorder=TRUE
MouseCursor=TRUE
ShowObjectEdge=FALSE
ChangeMouseCursor=TRUE
HideTaskBar=0
EnableToolTips=1
AutoScreenScalingResolution=1920 1080
VKHint=FALSE
VKMinMax=FALSE
TouchScale=100
TouchSystemDefault=Keypad
TouchSystemDefaultDLL=IndVkStd.DLL
SavePicturesInSeparateFile=1
EnableEnhancedGraphics=1
Touch=FALSE
AutoScreenScaling=1
StartupScreen=2Login.scr
Description=Project Name
VKEnableMultiLineTextInput=TRUE
[Options]
ConfigurationFlags={07BB6393-5F27-407B-9014-CE80B8335BE1}
EnableTranslate=1
IgnoreSpaceTranslate=1
KeepOriginalTranslate=1
ResolveOrderTranslate=1
VersionID=1
UniSoftVersion=7.1
ServicePack=3
BuildNumber=1701.1604.1402.0000
BooleanTrueAboveZero=0
DriverAndOPC=1
OpenIncWidthHeight=0
EnableTagNameOnRunGlobalProcedureOnTag=1
KeyboardLanguage=EN
EnableAlgorithmIncreaseBitmap=1
GridHorz=5
GridVert=5
GridEnableSnap=0
GridColor=8388608
GridVisible=1
ShowAllProperties=TRUE
ShowRemoteDebuggingToolsUsersWarning=FALSE
[Info]
ProductID=30
CompanyID=0
AppResolution=1920 1080
AppTemplate=Empty Application
[Script]
VBBoolean=1
TruncRealToInt=1
[OPC]
ExpandTag=1
[Alarm]
UseRateTimeUnit=1
AddEventTimeColumn=1
FileLifeTime=5
Format=0
CustomFields=0
[TCPIPFields]
Count=2
FieldName0=Quality
FieldName1=Timestamp
FieldStatus0=1
FieldStatus1=1
[StartupTasks]
AllowTCPServerRemote=1
CheckWinCE=1
Count=4
Task0=BGTASK
Task1=TCPSERVER
Task2=DRIVER
Task3=VIEWER
[Compatibility]
EnforceViewerWeb=0
[SoftPLC]
Enable=FALSE
[Web]
Touch=0
HostIPAddress=127.0.0.1
DisableRemoteClientCommands=0
EnableToolTips=1
SendPeriod=100
EnableLog=0
CompressFiles=1
AutoScreenScaling=1
VKHint=0
VKMinMax=0
TouchScale=100
TouchSystemDefault=Keypad
TouchSystemDefaultDLL=IndVkStd.DLL
[Security]
CaseSensitivePassword=1
[Objects]
Command-AlwaysForceChecked=0
CommandExecutionType=1
ExecCmdOnlyObjOnTop=0
TextPopupInput=0
EnableObjectFocus=1
[Execution Environment]
Station=1
SerialPort=0
SerialBaud=19200
SerialRTS=0
SerialCTS=0
IPAddress=10.100.1.20
RemoteAppDir=HMI\
SystemFilesOnlyNewerFiles=TRUE
SendToTargetOnlyNewerFiles=TRUE
KeepUserFiles=FALSE
CurrentUser=administrator
[DB 1]
Count=38
Exp1=infTRAYexit.CTL_boALM
Continuous1=1
Exp2=infTRAYexit.CTL_boALMret
Continuous2=1
Exp3=infCBRentryDOR.CTL_boALM
Continuous3=1
Exp4=infCBRentryDOR.CTL_boALMopn
Continuous4=1
Exp5=infCBRentryDOR.CTL_boALMcls
Continuous5=1
Exp6=infCBRexitDOR.CTL_boALM
Continuous6=1
Exp7=infCBRexitDOR.CTL_boALMopn
Continuous7=1
Exp8=infCBRexitDOR.CTL_boALMcls
Continuous8=1
Exp9=inGCIPpreRINSEseqNUM
Continuous9=1
Exp10=UserName
Continuous10=1
Exp11=NoInputTim
Continuous11=1
Exp12=infCBRBallastVacVLV.CTL_boHAutoMan
Continuous12=1
Exp13=infCBRBallastVacVLV.CTL_boHManOPN
Continuous13=1
Exp14=infCBRBallastVacVLV.CTL_boHManCLS
Continuous14=1
Exp15=infCBRMainCbrVLV.CTL_boHAutoMan
Continuous15=1
Exp16=infCBRMainCbrVLV.CTL_boHManOPN
Continuous16=1
Exp17=infCBRMainCbrVLV.CTL_boHManCLS
Continuous17=1
Exp18=outfCBRBallastVacVLV.CTL_boHAutoMan
Continuous18=1
Exp19=outfCBRBallastVacVLV.CTL_boHManOPN
Continuous19=1
Exp20=outfCBRBallastVacVLV.CTL_boHManCLS
Continuous20=1
Exp21=outfCBRMainCbrVLV.CTL_boHAutoMan
Continuous21=1
Exp22=outfCBRMainCbrVLV.CTL_boHManOPN
Continuous22=1
Exp23=outfCBRMainCbrVLV.CTL_boHManCLS
Continuous23=1
Exp24=Conv.reSPDfbk
Continuous24=1
Exp25=inGhPSRsclMIN
Continuous25=1
Exp26=iGhHeaterOnTime_m
Continuous26=1
Exp27=iGhHeaterOffTime_m
Continuous27=1
Exp28=strCommAddress
Continuous28=1
Exp29=strPCAmsNetID
Continuous29=1
Exp30=Conv.reHManSPD
Continuous30=1
Exp31=Conv.reHAutoSPD
Continuous31=1
Exp32=irotation
Continuous32=1
Exp33=iGhRelPsr
Continuous33=1
Exp34=iGhRelFrntTankTemp
Continuous34=1
Exp35=iGhRelRearTankTemp
Continuous35=1
Exp36=iGLinearDrivePos
Continuous36=1
Exp37=bGHomeRequest
Continuous37=1
Exp38=Conv.boHAutoman
Continuous38=1
Exp39=Conv.ulPLScnt
Continuous39=1
Exp40=boGoutfCMBRfull
Continuous40=1
Exp41=boTRAYexit
Continuous41=1
Exp42=inGhBELTtoothALMspMIN
Continuous42=1
[DB 2]
Count=24
Exp1=LCRpsCom[1].boDOhvENB
Continuous1=1
Exp2=LCRpsCom[2].boDOhvENB
Continuous2=1
Exp3=LCRpsCom[3].boDOhvENB
Continuous3=1
Exp4=LCRpsCom[4].boDOhvENB
Continuous4=1
Exp5=LCRpsCom[5].boDOhvENB
Continuous5=1
Exp6=LCRpsCom[6].boDOhvENB
Continuous6=1
Exp7=LCRpsCom[7].boDOhvENB
Continuous7=1
Exp8=LCRpsCom[8].boDOhvENB
Continuous8=1
Exp9=LCRpsCom[9].boDOhvENB
Continuous9=1
Exp10=LCRpsCom[10].boDOhvENB
Continuous10=1
Exp11=LCRpsCom[11].boDOhvENB
Continuous11=1
Exp12=LCRpsCom[12].boDOhvENB
Continuous12=1
Exp13=LCRpsCom[13].boDOhvENB
Continuous13=1
Exp14=LCRpsCom[14].boDOhvENB
Continuous14=1
Exp15=LCRpsCom[15].boDOhvENB
Continuous15=1
Exp16=LCRpsCom[16].boDOhvENB
Continuous16=1
Exp17=LCRpsCom[17].boDOhvENB
Continuous17=1
Exp18=LCRpsCom[18].boDOhvENB
Continuous18=1
Exp19=LCRpsCom[19].boDOhvENB
Continuous19=1
Exp20=LCRpsCom[20].boDOhvENB
Continuous20=1
Exp21=strRCPRecipePath
Continuous21=1
Exp22=strRCPFileName
Continuous22=1
Exp23=TANKfrnt.boDIlowLVL
Continuous23=1
Exp24=TANKrear.boDIlowLVL
Continuous24=1
Exp25=infCBRexitDOR.IO_boDIopnSTS
Continuous25=1
Exp26=infCBRexitDOR.IO_boDIclsSTS
Continuous26=1
Exp27=outfCBRentryDOR.CTL_boALM
Continuous27=1
Exp28=outfCBRentryDOR.IO_boDIopnSTS
Continuous28=1
Exp29=outfCBRentryDOR.IO_boDIclsSTS
Continuous29=1
Exp30=outfCBRexitDOR.CTL_boALM
Continuous30=1
Exp31=outfCBRexitDOR.IO_boDIopnSTS
Continuous31=1
Exp32=outfCBRexitDOR.IO_boDIclsSTS
Continuous32=1
Exp33=frntEDUvlv.IO_boDOopn
Continuous33=1
Exp34=rearEDUvlv.IO_boDOopn
Continuous34=1
Exp35=tankREARchillerVLV.IO_boDOopn
Continuous35=1
Exp36=tankFRNTchillerVLV.IO_boDOopn
Continuous36=1
Exp37=outfPULLcyl.CTL_inPOScmdMM
Continuous37=1
Exp38=infPUSHcyl.CTL_inPOScmdMM
Continuous38=1
Exp39=outfPULLcyl.CTL_boHAutoMan
Continuous39=1
Exp40=infPUSHcyl.CTL_boHAutoMan
Continuous40=1
Exp41=infTRAYins.CTL_boHAutoMan
Continuous41=1
Exp42=infTRAYexit.CTL_boHAutoMan
Continuous42=1
Exp43=outfTRAYins.CTL_boHAutoMan
Continuous43=1
Exp44=outfTRAYexit.CTL_boHAutoMan
Continuous44=1
Exp45=infPUSHcyl.CTL_inPOS
Continuous45=1
Exp46=infPUSHcyl.CTL_inHextSP
Continuous46=1
Exp47=infPUSHcyl.CTL_inHretSP
Continuous47=1
Exp48=outfPullcyl.CTL_inHextSP
Continuous48=1
[DB 3]
Count=24
Exp1=FANfrntRIGHT.boHManSTR
Continuous1=1
Exp2=FANfrntRIGHT.boHManSTP
Continuous2=1
Exp3=FANrearRIGHT.boHManSTR
Continuous3=1
Exp4=FANrearRIGHT.boHManSTP
Continuous4=1
Exp5=FANfrntLEFT.boHManSTR
Continuous5=1
Exp6=FANfrntLEFT.boHManSTP
Continuous6=1
Exp7=FANrearLEFT.boHManSTR
Continuous7=1
Exp8=infPUSHcyl.CTL_boINextPOS
Continuous8=1
Exp9=infPUSHcyl.CTL_boINretPOS
Continuous9=1
Exp10=outfPULLcyl.CTL_boINextPOS
Continuous10=1
Exp11=outfPULLcyl.CTL_boINretPOS
Continuous11=1
Exp12=infCBRentryDOR.CTL_boALMopn
Continuous12=1
Exp13=infCBRMainCbrVLV.CTL_boHAutoMan
Continuous13=1
Exp14=infCBRMainCbrVLV.CTL_boHManOPN
Continuous14=1
Exp15=infCBRMainCbrVLV.CTL_boHManCLS
Continuous15=1
Exp16=infCBRBallastVacVLV.CTL_boHAutoMan
Continuous16=1
Exp17=infCBRBallastVacVLV.CTL_boHManOPN
Continuous17=1
Exp18=infCBRBallastVacVLV.CTL_boHManCLS
Continuous18=1
Exp19=outfCBRMainCbrVLV.CTL_boHAutoMan
Continuous19=1
Exp20=outfCBRMainCbrVLV.CTL_boHManOPN
Continuous20=1
Exp21=outfCBRMainCbrVLV.CTL_boHManCLS
Continuous21=1
Exp22=outfCBRBallastVacVLV.CTL_boHAutoMan
Continuous22=1
Exp23=outfCBRBallastVacVLV.CTL_boHManCLS
Continuous23=1
Exp24=outfCBRBallastVacVLV.CTL_boHManOPN
Continuous24=1
Exp25=boDIGutterEstop
Continuous25=1
Exp26=inGhHTRtimeRMN
Continuous26=1
Exp27=boGYLWlightBatchREQ
Continuous27=1
Exp28=boGDIwtrHlvlSW
Continuous28=1
Exp29=boGRCPlddPMV
Continuous29=1
Exp30=inGNofLCRenbANDauto
Continuous30=1
Exp31=boGlcrLOCerror
Continuous31=1
Exp32=boGhCIPAutoRUNpmv
Continuous32=1
Exp33=inMagHEATexTemp
Continuous33=1
Exp34=boGtrayWAITwng
Continuous34=1
Exp35=boGtrayWAITalm
Continuous35=1
Exp36=boGbeltoothALM
Continuous36=1
Exp37=inAIGMagHEATexTemp
Continuous37=1
Exp38=infPUSHcyl.CTL_inPOScmdMM
Continuous38=1
Exp39=outfPULLcyl.CTL_boHAutoMan
Continuous39=1
Exp40=infPUSHcyl.CTL_boHAutoMan
Continuous40=1
Exp41=infTRAYins.CTL_boHAutoMan
Continuous41=1
Exp42=infTRAYexit.CTL_boHAutoMan
Continuous42=1
Exp43=outfTRAYins.CTL_boHAutoMan
Continuous43=1
Exp44=outfTRAYexit.CTL_boHAutoMan
Continuous44=1
Exp45=conv.bohAutoman
Continuous45=1
Exp46=Conv.bohManSTR
Continuous46=1
Exp47=Conv.bohManSTP
Continuous47=1
Exp48=Conv.reHManSPD
Continuous48=1
Exp49=boGhBPStopENC
Continuous49=1
Exp50=outfCBRvacPMP.boHManSTR
Continuous50=1
Exp51=outfCBRvacPMP.boHManSTP
Continuous51=1
Exp52=outfCBRvacPMP.boHAutoman
Continuous52=1
Exp53=infCBRvacPMP.boHManSTR
Continuous53=1
Exp54=infCBRvacPMP.boHManSTP
Continuous54=1
Exp55=infCBRvacPMP.boHAutoman
Continuous55=1
Exp56=EDfrnt.boHManSTR
Continuous56=1
Exp57=EDfrnt.boHmanSTP
Continuous57=1
Exp58=EDfrnt.inSPDcmd
Continuous58=1
Exp59=EDfrnt.boHALMrst
Continuous59=1
Exp60=EDfrnt.boHautoman
Continuous60=1
Exp61=EDrear.boHManSTR
Continuous61=1
Exp62=EDrear.boHmanSTP
Continuous62=1
Exp63=EDrear.inSPDcmd
Continuous63=1
Exp64=EDrear.boHALMrst
Continuous64=1
Exp65=EDrear.boHautoman
Continuous65=1
Exp66=boGhBPStopENC
Continuous66=1
[DB 4]
Count=39
Exp1=infCBRentryDOR.CTL_boALM
Continuous1=1
Exp2=infCBRentryDOR.CTL_boALMopn
Continuous2=1
Exp3=infCBRentryDOR.CTL_boALMcls
Continuous3=1
Exp4=infCBRexitDOR.CTL_boALM
Continuous4=1
Exp5=infCBRexitDOR.CTL_boALMopn
Continuous5=1
Exp6=infCBRexitDOR.CTL_boALMcls
Continuous6=1
Exp7=infTRAYexit.CTL_boALM
Continuous7=1
Exp8=infTRAYexit.CTL_boALMext
Continuous8=1
Exp9=infTRAYexit.CTL_boALMret
Continuous9=1
Exp10=outfCBRentryDOR.CTL_boALM
Continuous10=1
Exp11=outfCBRentryDOR.CTL_boALMopn
Continuous11=1
Exp12=outfCBRentryDOR.CTL_boALMcls
Continuous12=1
Exp13=outfCBRexitDOR.CTL_boALM
Continuous13=1
Exp14=outfCBRexitDOR.CTL_boALMopn
Continuous14=1
Exp15=outfCBRexitDOR.CTL_boALMcls
Continuous15=1
Exp16=outfTRAYunl.CTL_boALM
Continuous16=1
Exp17=outfTRAYunl.CTL_boALMext
Continuous17=1
Exp18=outfTRAYunl.CTL_boALMret
Continuous18=1
Exp19=outfTRAYins.CTL_boALM
Continuous19=1
Exp20=outfTRAYins.CTL_boALMext
Continuous20=1
Exp21=outfTRAYins.CTL_boALMret
Continuous21=1
Exp22=outfTRAYexit.CTL_boALM
Continuous22=1
Exp23=outfTRAYexit.CTL_boALMext
Continuous23=1
Exp24=outfTRAYexit.CTL_boALMret
Continuous24=1
Exp25=bPreOpChk[1]
Continuous25=1
Exp26=bPreOpChk
Continuous26=1
Exp27=boGinfSRVdriverREADY
Continuous27=1
Exp28=boGVLAutoPMV
Continuous28=1
Exp29=bPreOpChk[9]
Continuous29=1
Exp30=bPreOpChk[10]
Continuous30=1
Exp31=bPreOpChk[11]
Continuous31=1
Exp32=bPreOpChk[12]
Continuous32=1
Exp33=TANKfrntCHILLERpmp.boDIaux
Continuous33=1
Exp34=TANKrearCHILLERpmp.boDIaux
Continuous34=1
Exp35=chillerEXpmp.boDIaux
Continuous35=1
Exp36=HTRfrntTop.boDIAUX
Continuous36=1
Exp37=HTRrearTop.boDIAUX
Continuous37=1
Exp38=boGVLAutoPMV
Continuous38=1
Exp39=boGAutoRunPMV
Continuous39=1
Exp40=strPowerUPTime[3]
Continuous40=1
Exp41=strPowerUPTime[4]
Continuous41=1
Exp42=strPowerUPTime[5]
Continuous42=1
Exp43=strPowerUPTime[6]
Continuous43=1
Exp44=outfcbrvacpmp.CTL_boALM
Continuous44=1
Exp45=infCBRvacPMP.boDIaux
Continuous45=1
Exp46=infcbrvacpmp.CTL_boALM
Continuous46=1
Exp47=infCBRprgvlv.CTL_boALM
Continuous47=1
Exp48=strcommaddress
Continuous48=1
Exp49=boGtrayATinf
Continuous49=1
[Replace]
MethodReplace=-1
FilterReplace=0
StringValueColumnWidth0=289
StringValueColumnWidth1=175
WholeTagNameColumnWidth0=175
WholeTagNameColumnWidth1=197
TagLevelColumnWidth0=175
TagLevelNameColumnWidth1=175
MainTagNameColumnWidth0=175
MainTagNameColumnWidth1=175
ArrayIndexColumnWidth0=175
ArrayIndexColumnWidth1=175
ClassMemberColumnWidth0=266
ClassMemberColumnWidth1=175
TagFieldColumnWidth0=175
TagFieldColumnWidth1=175
PropertyColumnWidth0=175
PropertyColumnWidth1=175
CurrentTab=1
GlobalColumnWidth0=175
GlobalColumnWidth1=175
[EventLogger]
CustomFields=0
Col0Width=1
Col1Width=255
Col2Width=64
Col3Width=375
[Touch]
DisablePalmRejection=1
[TCP]
Port=1234
SendPeriod=100
EnableBinaryControl=0
PreloadTagsTimeOnRemote=3000
PreloadTagsTimeOnLocal=1000
[ExecutionEnvironment]
Timeout=30000
EnableFileCompression=0
[Preferences]
WarningDownloadScreen=1
ReplaceTagAppTags=0
ConfirmChangeScreenPositionSize=1
ResetTagsDatabase=0
WorksheetReplaceConfirmation=1
WarningAfterSavingSymbols=1
AutoReloadAppOnClientsDatabaseChanged=0
[Dump]
EnableDebugFile=1
EnableExceptionDumpFile=0
EnableLogMemory=0
UseSmallDump=1
[AppBuilder]
TrackerSize=3
[Logwin]
FieldRead=1
FieldWrite=1
EnableDateTime=FALSE
TagCount=0
RecipeCommands=0
TraceMessages=1
ScreenOpenClose=0
Security=0
DDEMessages=0
Serial=1
TCPMessages=0
OpcDaLog=0
Database=0
[Object]
GridColumnWidth0=46
GridColumnWidth1=182
GridColumnWidth2=102
GridColumnWidth3=73
GridColumnWidth4=55
GridColumnWidth5=73
GridColumnWidth7=109
GridColumnWidth8=80
[UsedDrivers]
Count=1
Task0=Driver TWCAT
[Menu1]
File=FALSE
Open=FALSE
Close=FALSE
Print=FALSE
PrintSetup=FALSE
Exit=FALSE
[Menu2]
Security=FALSE
Login=FALSE
Logout=FALSE
Info=FALSE
[Menu4]
Tools=TRUE
DatabaseSpy=TRUE
LogWin=TRUE
[Library]
SelectedLibrary=C:\Users\Kevin\Desktop\Program\NewWalnut190214\NewWalnut190214HMI\symbol
[EditCustomProperties]
CustomProperty_ColumnWidth_S0=94
CustomProperty_ColumnWidth_S1=269
CustomProperty_ColumnWidth_E0=188
CustomProperty_ColumnWidth_E1=330
[CNP]
Data=89A864BEAE4EE4D4D6396FD5A4C1155E7884446D1163C49E763233913FE23AFCABFB834161B53788DA44F6490600A6FED87DE4CBF38C60157D5FD2F9E81B6CC462BCF7BC55836287D776FA124781BEB79D7CDB67C44C084A31B5C06B2AA97A2E
[Search]
State=01000000D08C9DDF0115D1118C7A00C04FC297EB0100000074F7505EA66FC3478F0EAB025405FCAF0000000002000000000003660000C000000010000000298502BA23F0C0B100DE9D93C54C780F0000000004800000A000000010000000E0B166B3E209DC311CD559CC0A6130765000000022916EE1D1E4BE30264BDEA2E41188E624BB01BC8293EB6FC04EDC12DB1EBD11903D4BB9B887195FEF0C689BCEB06303FA9D79197BBCD24F94ACFE8F169F60465FD17CEA1232ED20496A85FD4B600AAE1400000040017130B9369DFD52CE8F0A4595DB50400220E7
